package intern

import (
	"context"
	"fmt"

	"github.com/ShaileshSurya/hammer"
	ptypes "gitlab.com/o-cloud/catalog/api/public"
	"gitlab.com/o-cloud/sbotel/hammerpropagator"
)

type CatalogClient struct {
	baseURL string
	client  *hammer.Hammer
}

func NewClient(url string) InternalCatalogAPI {
	return &CatalogClient{
		baseURL: url,
		client:  hammer.New(),
	}
}

func (c *CatalogClient) RegisterDataProvider(ctx context.Context, service ProviderInternalInfo) error {
	url := fmt.Sprintf("%s/%s/register", c.baseURL, service.Type)
	req, _ := hammer.RequestBuilder().
		Post().
		WithURL(url).
		WithRequestBody(service).
		Build()
	hammerpropagator.Propagate(ctx, req)
	_, err := c.client.Execute(req)
	return err
}

func (c *CatalogClient) SearchExistingProvider(ctx context.Context, service ProviderInternalInfo) (ProviderInternalInfo, error) {
	url := fmt.Sprintf("%s/%s/provider/search", c.baseURL, service.Type)
	var ret ProviderInternalInfo
	req, _ := hammer.RequestBuilder().
		Post().
		WithURL(url).
		WithRequestBody(service).
		Build()
	hammerpropagator.Propagate(ctx, req)
	err := c.client.ExecuteInto(req, &ret)
	return ret, err
}

func (c *CatalogClient) RemoveProvider(ctx context.Context, providerType ptypes.ProviderType, ProviderId string) error {
	url := fmt.Sprintf("%s/%s/provider/%s", c.baseURL, providerType, ProviderId)
	req, _ := hammer.RequestBuilder().
		Delete().
		WithURL(url).
		Build()
	hammerpropagator.Propagate(ctx, req)
	_, err := c.client.Execute(req)
	return err
}

func (c *CatalogClient) GetProviderVisibility(ctx context.Context, providerType ptypes.ProviderType, ProviderId string) ([]string, error) {
	url := fmt.Sprintf("%s/%s/provider/%s/visibility", c.baseURL, providerType, ProviderId)
	var ret []string
	req, _ := hammer.RequestBuilder().
		Get().
		WithURL(url).
		Build()
	hammerpropagator.Propagate(ctx, req)
	err := c.client.ExecuteInto(req, &ret)
	return ret, err
}

func (c *CatalogClient) AddProviderVisibility(ctx context.Context, providerType ptypes.ProviderType, ProviderId string, visibility []string) error {

	url := fmt.Sprintf("%s/%s/provider/%s/visibility", c.baseURL, providerType, ProviderId)
	req, _ := hammer.RequestBuilder().
		Post().
		WithURL(url).
		Build()
	hammerpropagator.Propagate(ctx, req)
	_, err := c.client.Execute(req)
	return err
}

func (c *CatalogClient) RemoveProviderVisibility(ctx context.Context, providerType ptypes.ProviderType, ProviderId string, visibility []string) error {

	url := fmt.Sprintf("%s/%s/provider/%s/visibility", c.baseURL, providerType, ProviderId)
	req, _ := hammer.RequestBuilder().
		Delete().
		WithURL(url).
		Build()
	hammerpropagator.Propagate(ctx, req)
	_, err := c.client.Execute(req)
	return err
}
