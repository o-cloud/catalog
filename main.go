package main

import (
	"log"

	"gitlab.com/o-cloud/catalog/config"
	"gitlab.com/o-cloud/catalog/service"
	"go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin"

	"github.com/gin-gonic/gin"
)

func main() {
	config.Load()
	r := setupRouter()

	if err := r.Run(config.Config.Server.ListenAddress); err != nil {
		log.Println(err)
	}
}

func setupRouter() *gin.Engine {
	r := gin.Default()

	r.Use(otelgin.Middleware("CatalogService"))

	catalogHandler := service.NewCatalogHandler()
	catalogHandler.SetupPublicRoutes(r.Group(config.Config.Server.PublicEndpoint + "/public"))
	catalogHandler.SetupInternalRoutes(r.Group(config.Config.Server.PrivateEndpoint + "/internal"))

	return r
}
