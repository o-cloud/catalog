package service

import (
	"context"

	outypes "gitlab.com/o-cloud/catalog/api/public"
	"go.opentelemetry.io/otel"
)

// Public API functions

func (cs *CatalogService) Search(ctx context.Context, providerType outypes.ProviderType, searchTerms, originPeer string) ([]outypes.ProviderInfo, error) {
	ctx, span := otel.Tracer("catalog/service").Start(ctx, "Search")
	defer span.End()
	return cs.MongoService.Search(ctx, providerType, searchTerms, originPeer), nil
}

func (cs *CatalogService) GetProviderInfo(ctx context.Context, providerType outypes.ProviderType, ProviderId string) (outypes.ProviderInfo, error) {
	ctx, span := otel.Tracer("catalog/service").Start(ctx, "GetProviderInfo")
	defer span.End()
	providerFound, err := cs.MongoService.GetProvider(ctx, providerType, ProviderId)
	result := *providerFound.ToPublicApi()
	result.Type = providerType
	return result, err
}
