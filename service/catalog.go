package service

type CatalogService struct {
	MongoService ICatalogMongoService
}

func NewCatalogProvider() *CatalogService {
	mongoService := NewCatalogMongoService()
	cp := &CatalogService{MongoService: mongoService}

	mongoService.InitCollections()

	return cp
}
