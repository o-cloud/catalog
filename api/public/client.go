package public

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/propagation"
)

type CatalogClient struct {
	baseURL string
	client  *http.Client
}

func NewClient(url string) CatalogueApi {
	return &CatalogClient{
		baseURL: url,
		client:  &http.Client{},
	}
}

func (c *CatalogClient) Search(ctx context.Context, providerType ProviderType, searchTerms, originPeer string) ([]ProviderInfo, error) {
	url := fmt.Sprintf("%s/%s/search", c.baseURL, providerType)
	if len(searchTerms) > 0 {
		url += "?terms=" + searchTerms
	}
	var ret []ProviderInfo
	body, err := c.doGetWithHeaders(ctx, url, map[string]string{
		HEADER_ORIGIN_PEER: originPeer,
	})
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(body, &ret)
	return ret, err
}

func (c *CatalogClient) GetProviderInfo(ctx context.Context, providerType ProviderType, providerId string) (ProviderInfo, error) {
	url := fmt.Sprintf("%s/%s/provider/%s/info", c.baseURL, providerType, providerId)
	var ret ProviderInfo
	body, err := c.doGet(ctx, url)
	if err != nil {
		return ret, err
	}
	err = json.Unmarshal(body, &ret)
	return ret, err
}

func (c *CatalogClient) doGetWithHeaders(ctx context.Context, url string, headers map[string]string) ([]byte, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	// Propagate headers
	for k, v := range headers {
		req.Header.Add(k, v)
	}

	otel.GetTextMapPropagator().Inject(ctx, propagation.HeaderCarrier(req.Header))
	bod, err := c.doRequest(req)
	return bod, err
}

func (c *CatalogClient) doGet(ctx context.Context, url string) ([]byte, error) {
	return c.doGetWithHeaders(ctx, url, make(map[string]string))
}

func (c *CatalogClient) doRequest(req *http.Request) ([]byte, error) {
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode >= 200 || resp.StatusCode < 300 {
		return body, nil
	}
	return nil, fmt.Errorf("%s", body)
}
