package config

import (
	"log"

	"github.com/spf13/viper"
	"gitlab.com/o-cloud/sbotel"
	"gitlab.com/o-cloud/sbotel/viperconf"
)

var (
	Config ProviderConfig
)

const (
	dockerConfigPath = "/etc/irtsb/"
	localConfigPath  = "./"
)

func Load() {

	//Environment variables
	viper.SetDefault("ServiceName", "Catalog")
	viper.BindEnv("ServiceName", "SERVICE_NAME")

	viper.SetDefault("MONGO_CON_STRING", "mongodb://localhost:27017/")
	viper.BindEnv("MONGO_CON_STRING")

	viper.SetConfigType("yaml")
	viper.AddConfigPath(dockerConfigPath)
	viper.AddConfigPath(localConfigPath)
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("Unable to read config file", err)
	}
	// Decode config into ProviderConfig
	if err := viper.Unmarshal(&Config); err != nil {
		log.Fatal("unable to decode configuration", err)
	}

	//Config otel
	if config, err := viperconf.New().WithFile(dockerConfigPath + "otel.yaml").GetConfig(); err != nil {
		log.Print("Error reading otel config file", err)
	} else {
		config.ServiceName = viper.GetString("ServiceName")
		sbotel.InitConfiguration(config)
	}
}

type ProviderConfig struct {
	ProviderName string
	MongoDbUri   string
	Server       ServerProviderConfig
}

type ServerProviderConfig struct {
	ListenAddress   string
	PublicEndpoint  string
	PrivateEndpoint string
}
