package service

import (
	"context"

	intypes "gitlab.com/o-cloud/catalog/api/intern"
	outypes "gitlab.com/o-cloud/catalog/api/public"
	"go.opentelemetry.io/otel"

	"go.mongodb.org/mongo-driver/mongo"
)

type OriginID int

const (
	ORIGIN_UNKNOWN  OriginID = iota // Anonymous request. Not identified
	ORIGIN_LOCAL                    // Request from our own cluster
	ORIGIN_EXTERNAL                 // Request from other peer cluster
)

// TODO: In the future, this should check cryptographic stuff to verify
// the origin of the request
func detectRequestOrigin(originPeer string) OriginID {
	switch originPeer {
	case "":
		return ORIGIN_UNKNOWN
	case "local":
		return ORIGIN_LOCAL
	default:
		return ORIGIN_EXTERNAL
	}
}

// Internal API functions

func (cs *CatalogService) RegisterDataProvider(ctx context.Context, provider intypes.ProviderInternalInfo) error {
	ctx, span := otel.Tracer("catalog/service").Start(ctx, "RegisterDataProvider")
	defer span.End()
	in := ProviderInternalInfo{}
	in.FromInternalApi(&provider)
	return cs.MongoService.SaveDataProvider(ctx, provider.Type, in)
}

func (cs *CatalogService) SearchExistingProvider(ctx context.Context, provider intypes.ProviderInternalInfo) (intypes.ProviderInternalInfo, error) {
	ctx, span := otel.Tracer("catalog/service").Start(ctx, "SearchExistingProvider")
	defer span.End()
	providerFound, err := cs.MongoService.GetProvider(ctx, provider.Type, provider.ProviderId)
	if err == mongo.ErrNoDocuments {
		return provider, err
	}
	prov := *providerFound.ToInternalApi()
	prov.Type = provider.Type
	return prov, nil
}

func (cs *CatalogService) RemoveProvider(ctx context.Context, providerType outypes.ProviderType, ProviderId string) error {
	ctx, span := otel.Tracer("catalog/service").Start(ctx, "RemoveProvider")
	defer span.End()
	return cs.MongoService.RemoveProvider(ctx, providerType, ProviderId)
}

func (cs *CatalogService) GetProviderVisibility(ctx context.Context, providerType outypes.ProviderType, ProviderId string) ([]string, error) {
	ctx, span := otel.Tracer("catalog/service").Start(ctx, "GetProviderVisibility")
	defer span.End()
	prov, err := cs.MongoService.GetProvider(ctx, providerType, ProviderId)
	if err != nil {
		return nil, err
	}
	return prov.Visibility, nil
}

func (cs *CatalogService) AddProviderVisibility(ctx context.Context, providerType outypes.ProviderType, ProviderId string, visibility []string) error {
	ctx, span := otel.Tracer("catalog/service").Start(ctx, "AddProviderVisibility")
	defer span.End()
	prov, err := cs.MongoService.GetProvider(ctx, providerType, ProviderId)
	if err != nil {
		return err
	}

	newVisibility := prov.Visibility

	//FIXME: Bad pattern (goto like)
insertloop:
	for _, insert := range visibility {
		for _, existing := range prov.Visibility {
			if insert == existing {
				continue insertloop
			}
		}
		newVisibility = append(newVisibility, insert)
	}

	err = cs.MongoService.UpdateVisibility(ctx, providerType, ProviderId, newVisibility)
	return err
}

func (cs *CatalogService) RemoveProviderVisibility(ctx context.Context, providerType outypes.ProviderType, ProviderId string, visibility []string) error {
	ctx, span := otel.Tracer("catalog/service").Start(ctx, "GetProviderInfo")
	defer span.End()
	prov, err := cs.MongoService.GetProvider(ctx, providerType, ProviderId)
	if err != nil {
		return err
	}
	newVisibility := []string{}

	//FIXME: Bad pattern (goto like)
removeloop:
	for _, existing := range prov.Visibility {
		for _, remove := range visibility {
			if remove == existing {
				continue removeloop
			}
		}
		newVisibility = append(newVisibility, existing)
	}
	err = cs.MongoService.UpdateVisibility(ctx, providerType, ProviderId, newVisibility)
	return err
}
