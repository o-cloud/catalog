package intern

import (
	"context"

	ptypes "gitlab.com/o-cloud/catalog/api/public"
)

type InternalCatalogAPI interface {
	RegisterDataProvider(ctx context.Context, service ProviderInternalInfo) error
	SearchExistingProvider(ctx context.Context, service ProviderInternalInfo) (ProviderInternalInfo, error)
	RemoveProvider(ctx context.Context, providerType ptypes.ProviderType, ProviderId string) error
	GetProviderVisibility(ctx context.Context, providerType ptypes.ProviderType, ProviderId string) ([]string, error)
	AddProviderVisibility(ctx context.Context, providerType ptypes.ProviderType, ProviderId string, visibility []string) error
	RemoveProviderVisibility(ctx context.Context, providerType ptypes.ProviderType, ProviderId string, visibility []string) error
}

type ProviderInternalInfo struct {
	Id                string                 `json:"id"`
	ProviderId        string                 `json:"providerId"`
	Visibility        []string               `json:"visibility" description:"A set of values that are allowed to see this resource"`
	Type              ptypes.ProviderType    `json:"providerType"`
	Version           string                 `json:"version"`
	Name              string                 `json:"name"`
	Description       string                 `json:"description"`
	Metadata          map[string]interface{} `json:"metadata"`
	EndpointLocation  string                 `json:"endpointLocation"`
	WorkflowStructure string                 `json:"workflowStructure"`
}
