package service

import (
	"net/http"

	intypes "gitlab.com/o-cloud/catalog/api/intern"
	outypes "gitlab.com/o-cloud/catalog/api/public"

	"github.com/gin-gonic/gin"
)

type CatalogHandler struct {
	PublicApi   outypes.CatalogueApi
	InternalApi intypes.InternalCatalogAPI
}

func NewCatalogHandler() *CatalogHandler {
	cs := NewCatalogProvider()
	return &CatalogHandler{PublicApi: cs, InternalApi: cs}
}

// SetupPublicRoutes Define the (sub)routes handled by JSONDecoratorHandler
func (C *CatalogHandler) SetupPublicRoutes(router *gin.RouterGroup) {
	router.GET("/:type/search", C.PubSearch)
	router.GET("/:type/provider/:providerId/info", C.PubGetProviderInfo)
}

// SetupInternalRoutes Define the (sub)routes handled by JSONDecoratorHandler
func (handler *CatalogHandler) SetupInternalRoutes(router *gin.RouterGroup) {
	router.POST("/:providerType/register", handler.RegisterDataProvider)
	router.POST("/:providerType/provider/search", handler.SearchExistingProvider)
	router.DELETE("/:providerType/provider/:providerId", handler.RemoveProvider)
	router.GET("/:providerType/provider/:providerId/visibility", handler.GetProviderVisibility)
	router.POST("/:providerType/provider/:providerId/visibility", handler.AddProviderVisibility)
	router.DELETE("/:providerType/provider/:providerId/visibility", handler.RemoveProviderVisibility)
}

//Public handlers

func (handler *CatalogHandler) PubSearch(c *gin.Context) {

	// Obtain the origin of the request
	//
	// TODO: This MUST use a reliable way to determine from where the request come (JWT? Https client auth? other?)
	// for now we just assume that the client never lies and identifies himself. We get the ID
	// from the header of the request. If no header, assume anonymous request

	originPeer := c.Request.Header.Get(outypes.HEADER_ORIGIN_PEER)

	results, err := handler.PublicApi.Search(c.Request.Context(), outypes.ProviderType(c.Param("type")), c.Query("terms"), originPeer)
	if err != nil {
		c.Error(err)
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.JSON(200, results)
}

func (handler *CatalogHandler) PubGetProviderInfo(c *gin.Context) {

	result, err := handler.PublicApi.GetProviderInfo(c.Request.Context(), outypes.ProviderType(c.Param("type")), c.Param("providerId"))

	if err != nil {
		c.Error(err)
		c.JSON(500, err)
		return
	}
	c.JSON(200, result)

}

// Internal Handlers

func (handler *CatalogHandler) RegisterDataProvider(c *gin.Context) {
	var provider intypes.ProviderInternalInfo
	if err := c.BindJSON(&provider); err != nil {
		c.Error(err)
		c.JSON(http.StatusUnprocessableEntity, err)
		return
	}
	err := handler.InternalApi.RegisterDataProvider(c.Request.Context(), provider)
	if err != nil {
		c.Error(err)
		c.JSON(http.StatusInternalServerError, err)
		return
	}
	c.String(http.StatusOK, "OK")
}

func (handler *CatalogHandler) SearchExistingProvider(c *gin.Context) {
	var provider intypes.ProviderInternalInfo
	if err := c.BindJSON(&provider); err != nil {
		c.Error(err)
		c.JSON(http.StatusUnprocessableEntity, err)
		return
	}
	res, err := handler.InternalApi.SearchExistingProvider(c.Request.Context(), provider)
	if err != nil {
		c.Error(err)
		c.JSON(500, err)
		return
	}
	c.JSON(200, res)
}

func (handler *CatalogHandler) RemoveProvider(c *gin.Context) {
	err := handler.InternalApi.RemoveProvider(c.Request.Context(), outypes.ProviderType(c.Param("providerType")), c.Param("providerId"))
	if err != nil {
		c.Error(err)
		c.JSON(500, err)
		return
	}
	c.Status(200)
}

func (handler *CatalogHandler) GetProviderVisibility(c *gin.Context) {
	res, err := handler.InternalApi.GetProviderVisibility(c.Request.Context(), outypes.ProviderType(c.Param("providerType")), c.Param("providerId"))
	if err != nil {
		c.Error(err)
		c.JSON(500, err)
		return
	}
	c.JSON(200, res)
}

func (handler *CatalogHandler) AddProviderVisibility(c *gin.Context) {
	var visibility []string

	if err := c.BindJSON(&visibility); err != nil {
		c.Error(err)
		c.JSON(http.StatusUnprocessableEntity, err)
		return
	}

	err := handler.InternalApi.AddProviderVisibility(c.Request.Context(), outypes.ProviderType(c.Param("providerType")), c.Param("providerId"), visibility)
	if err != nil {
		c.Error(err)
		c.JSON(500, err)
		return
	}
	c.String(200, "OK")
}

func (handler *CatalogHandler) RemoveProviderVisibility(c *gin.Context) {
	var visibility []string

	if err := c.BindJSON(&visibility); err != nil {
		c.Error(err)
		c.JSON(http.StatusUnprocessableEntity, err)
		return
	}

	err := handler.InternalApi.RemoveProviderVisibility(c.Request.Context(), outypes.ProviderType(c.Param("providerType")), c.Param("providerId"), visibility)
	if err != nil {
		c.Error(err)
		c.JSON(500, err)
		return
	}
	c.String(200, "OK")

}
