package service

import (
	"strings"

	intypes "gitlab.com/o-cloud/catalog/api/intern"
	outypes "gitlab.com/o-cloud/catalog/api/public"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ProviderInternalInfo struct {
	Id                primitive.ObjectID     `bson:"_id"`
	Visibility        []string               `bson:"visibility"`
	ProviderId        string                 `bson:"providerId"`
	Version           string                 `bson:"version"`
	Name              string                 `bson:"name"`
	Description       string                 `bson:"description"`
	Metadata          map[string]interface{} `bson:"metadata"`
	EndpointLocation  string                 `bson:"endpointLocation"`
	WorkflowStructure string                 `bson:"workflowStructure"`
}

func (sii *ProviderInternalInfo) ToInternalApi() *intypes.ProviderInternalInfo {
	return &intypes.ProviderInternalInfo{
		Id:                sii.Id.Hex(),
		ProviderId:        sii.ProviderId,
		Visibility:        sii.Visibility,
		Version:           sii.Version,
		Name:              sii.Name,
		Description:       sii.Description,
		Metadata:          sii.Metadata,
		EndpointLocation:  sii.EndpointLocation,
		WorkflowStructure: sii.WorkflowStructure,
	}
}

func (sii *ProviderInternalInfo) FromInternalApi(i *intypes.ProviderInternalInfo) {
	if strings.TrimSpace(i.Id) != "" {
		sii.Id, _ = primitive.ObjectIDFromHex(i.Id)
	} else {
		sii.Id = primitive.NewObjectID()
	}
	sii.ProviderId = i.ProviderId
	sii.Visibility = i.Visibility
	sii.Version = i.Version
	sii.Name = i.Name
	sii.Description = i.Description
	sii.Metadata = i.Metadata
	sii.EndpointLocation = i.EndpointLocation
	sii.WorkflowStructure = i.WorkflowStructure
}

func (sii *ProviderInternalInfo) ToPublicApi() *outypes.ProviderInfo {
	return &outypes.ProviderInfo{
		Id:                sii.Id.Hex(),
		ProviderId:        sii.ProviderId,
		Version:           sii.Version,
		Name:              sii.Name,
		Description:       sii.Description,
		Metadata:          sii.Metadata,
		EndpointLocation:  sii.EndpointLocation,
		WorkflowStructure: sii.WorkflowStructure,
	}
}
