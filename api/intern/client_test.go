package intern

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/ShaileshSurya/hammer"
)

type testSetup struct {
	t      *testing.T
	method string
	url    string
	ret    []byte
	cc     CatalogClient
}

func getSetup(t *testing.T) *testSetup {
	testSetup := &testSetup{t: t}
	testSetup.ret = []byte("{}")
	testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		testSetup.method = r.Method
		testSetup.url = r.RequestURI
		w.Write(testSetup.ret)
	}))
	testSetup.cc = CatalogClient{baseURL: testServer.URL, client: hammer.New().WithHTTPClient(testServer.Client())}
	return testSetup
}

func TestRegisterDataProvider(t *testing.T) {
	setup := getSetup(t)
	setup.cc.RegisterDataProvider(context.TODO(), ProviderInternalInfo{ProviderId: "provId", Type: "typ"})
	setup.AssertMethod(http.MethodPost)
	setup.AssertURL("/typ/register?")
}

func TestSearchExistingProvider(t *testing.T) {
	setup := getSetup(t)
	setup.cc.SearchExistingProvider(context.TODO(), ProviderInternalInfo{ProviderId: "provId", Type: "typ"})
	setup.AssertMethod(http.MethodPost)
	setup.AssertURL("/typ/provider/search?")
}

func TestRemoveProvider(t *testing.T) {
	setup := getSetup(t)
	setup.cc.RemoveProvider(context.TODO(), "typ", "provId")
	setup.AssertMethod(http.MethodDelete)
	setup.AssertURL("/typ/provider/provId?")
}

func TestGetProviderVisibility(t *testing.T) {
	setup := getSetup(t)
	setup.ret = []byte("[]")
	setup.cc.GetProviderVisibility(context.TODO(), "typ", "provId")
	setup.AssertMethod(http.MethodGet)
	setup.AssertURL("/typ/provider/provId/visibility?")
}

func TestAddProviderVisibility(t *testing.T) {
	setup := getSetup(t)
	setup.ret = []byte("[]")
	setup.cc.AddProviderVisibility(context.TODO(), "typ", "provId", []string{"vis"})
	setup.AssertMethod(http.MethodPost)
	setup.AssertURL("/typ/provider/provId/visibility?")
}

func TestRemoveProviderVisibility(t *testing.T) {
	setup := getSetup(t)
	setup.ret = []byte("[]")
	setup.cc.RemoveProviderVisibility(context.TODO(), "typ", "provId", []string{"vis"})
	setup.AssertMethod(http.MethodDelete)
	setup.AssertURL("/typ/provider/provId/visibility?")
}

func (ts *testSetup) AssertMethod(expectedMethod string) {
	if ts.method != expectedMethod {
		ts.t.Errorf("bad method: got %v want %v", ts.method, expectedMethod)
	}
}

func (ts *testSetup) AssertURL(expectedURL string) {
	if ts.url != expectedURL {
		ts.t.Errorf("bad URL: got %v want %v", ts.url, expectedURL)
	}
}
