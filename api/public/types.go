package public

import "context"

type ProviderType string

const (
	Data      ProviderType = "data"
	Service   ProviderType = "service"
	Computing ProviderType = "computing"
)

const HEADER_ORIGIN_PEER = "X-Origin-Peer"

type CatalogueApi interface {
	Search(ctx context.Context, providerType ProviderType, searchTerms, originPeer string) ([]ProviderInfo, error)
	GetProviderInfo(ctx context.Context, providerType ProviderType, providerId string) (ProviderInfo, error)
}

type ProviderInfo struct {
	Id                string                 `json:"id"`
	ProviderId        string                 `json:"providerId"`
	Type              ProviderType           `json:"providerType"`
	Version           string                 `json:"version"`
	Name              string                 `json:"name"`
	Description       string                 `json:"description"`
	Metadata          map[string]interface{} `json:"metadata"`
	EndpointLocation  string                 `json:"endpointLocation"`
	WorkflowStructure string                 `json:"workflowStructure"`
}
