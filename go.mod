module gitlab.com/o-cloud/catalog

go 1.16

require (
	github.com/ShaileshSurya/hammer v1.0.2
	github.com/gin-gonic/gin v1.7.2
	github.com/spf13/viper v1.8.1
	gitlab.com/o-cloud/sbotel v0.0.0-20211021091451-dc3eaeda7dcd
	gitlab.com/o-cloud/sbotel/hammerpropagator v0.0.0-20211021091451-dc3eaeda7dcd
	gitlab.com/o-cloud/sbotel/viperconf v0.0.0-20211022075011-9d04040dc1fb
	go.mongodb.org/mongo-driver v1.5.2
	go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin v0.22.0
	go.opentelemetry.io/otel v1.0.0-RC2
)
