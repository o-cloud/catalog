package service

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/spf13/viper"
	intypes "gitlab.com/o-cloud/catalog/api/intern"
	outypes "gitlab.com/o-cloud/catalog/api/public"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/x/bsonx"
	"go.opentelemetry.io/otel"
)

type ICatalogMongoService interface {
	Search(ctx context.Context, providerType outypes.ProviderType, searchTerms, originPeerName string) []outypes.ProviderInfo
	GetProvider(ctx context.Context, providerType outypes.ProviderType, ProviderId string) (ProviderInternalInfo, error)
	SaveDataProvider(ctx context.Context, providerType outypes.ProviderType, provider ProviderInternalInfo) error
	UpdateVisibility(ctx context.Context, providerType outypes.ProviderType, providerId string, visibility []string) error
	RemoveProvider(ctx context.Context, providerType outypes.ProviderType, providerId string) error
}

type CatalogMongoService struct {
	Database *mongo.Database
}

func NewCatalogMongoService() *CatalogMongoService {

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(viper.GetString("MONGO_CON_STRING")))
	if err != nil {
		panic("Erreur initialisation Client MongoDB")
	}
	cp := &CatalogMongoService{Database: client.Database(viper.GetString("ServiceName"))}

	return cp
}

func (ms *CatalogMongoService) SaveDataProvider(ctx context.Context, providerType outypes.ProviderType, provider ProviderInternalInfo) error {
	ctx, span := otel.Tracer("catalog/service").Start(ctx, "SaveDataProvider")
	defer span.End()
	insertCol := ms.Database.Collection(string(providerType))
	found, err := ms.GetProvider(ctx, providerType, provider.ProviderId)
	if err == nil {
		provider.Id = found.Id
		provider.Visibility = found.Visibility
		_, err = insertCol.ReplaceOne(ctx, bson.D{{Key: "_id", Value: provider.Id}}, provider)
	} else {
		_, err = insertCol.InsertOne(ctx, provider)
	}
	return err
}

func (ms *CatalogMongoService) RemoveProvider(ctx context.Context, providerType outypes.ProviderType, providerId string) error {
	ctx, span := otel.Tracer("catalog/service").Start(ctx, "RemoveProvider")
	defer span.End()
	collection := ms.Database.Collection(string(providerType))
	filter := bson.D{{Key: "providerId", Value: providerId}}
	_, err := collection.DeleteOne(ctx, filter)
	return err
}

func (ms *CatalogMongoService) UpdateVisibility(ctx context.Context, providerType outypes.ProviderType, providerId string, visibility []string) error {
	ctx, span := otel.Tracer("catalog/service").Start(ctx, "UpdateVisibility")
	defer span.End()
	insertCol := ms.Database.Collection(string(providerType))
	_, err := insertCol.UpdateOne(ctx,
		bson.M{"providerId": providerId},
		bson.D{
			{Key: "$set", Value: bson.D{{Key: "visibility", Value: visibility}}},
		})
	return err
}

func (ms *CatalogMongoService) SearchExistingProvider(ctx context.Context, provider intypes.ProviderInternalInfo) (intypes.ProviderInternalInfo, error) {
	ctx, span := otel.Tracer("catalog/service").Start(ctx, "SearchExistingProvider")
	defer span.End()

	providerSearch := &ProviderInternalInfo{}
	providerSearch.FromInternalApi(&provider)

	searchCol := ms.Database.Collection(string(provider.Type))
	var providerFound ProviderInternalInfo
	err := searchCol.FindOne(ctx, bson.D{{Key: "providerId", Value: providerSearch.ProviderId}}).Decode(&providerFound)
	if err == mongo.ErrNoDocuments {
		return *providerSearch.ToInternalApi(), err
	}
	return *providerFound.ToInternalApi(), nil
}

func (ms *CatalogMongoService) Search(ctx context.Context, providerType outypes.ProviderType, searchTerms, originPeerName string) []outypes.ProviderInfo {
	ctx, span := otel.Tracer("catalog/service").Start(ctx, "Search")
	defer span.End()
	searchCol := ms.Database.Collection(string(providerType))
	searchFilter := bson.M{}

	if len(searchTerms) > 0 {
		searchFilter = bson.M{
			"$text": bson.M{
				"$search": searchTerms,
			},
		}
	}

	//TODO: Use directly the tag from the entry instead of hardcoded values
	internalVisibilityTag := "visibility"
	switch detectRequestOrigin(originPeerName) {
	case ORIGIN_UNKNOWN:
		// Public search (no origin, so anonymous). Expose only elements without visibility set
		searchFilter[internalVisibilityTag] = nil
	case ORIGIN_EXTERNAL:
		// Identified peer. Search for public or where is authorized to search
		searchFilter["$or"] = bson.A{
			bson.M{internalVisibilityTag: nil},
			bson.M{internalVisibilityTag: originPeerName},
		}
	case ORIGIN_LOCAL:
		break // A local search should not filter and return all entries in the DB
	default:
		// Not sure about the case, so better not leak data
		log.Fatal("Request from " + originPeerName + "not recognized. Not returning any data")
		return nil
	}

	cur, err := searchCol.Find(ctx, searchFilter)
	if err != nil {
		log.Fatal(err)
		return nil
	}
	defer cur.Close(ctx)
	var curResults []ProviderInternalInfo
	cur.All(ctx, &curResults)
	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}
	results := make([]outypes.ProviderInfo, len(curResults))
	for i := 0; i < len(curResults); i++ {
		results[i] = *curResults[i].ToPublicApi()
		results[i].Type = providerType
	}
	return results
}

func (ms *CatalogMongoService) InitCollections() {
	fmt.Println("Init collections")
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	toInit := []outypes.ProviderType{"data", "service", "computing"}

	model := mongo.IndexModel{
		Keys:    bsonx.Doc{{Key: "name", Value: bsonx.String("text")}, {Key: "description", Value: bsonx.String("text")}},
		Options: options.Index().SetName("search"),
	}
insertingIndex:
	for _, v := range toInit {
		col := ms.Database.Collection(string(v))
		indexes := col.Indexes()
		cur, err := indexes.List(ctx)
		if err != nil {
			fmt.Println(err)
			continue
		}
		var results []bson.M
		if err = cur.All(context.TODO(), &results); err != nil {
			fmt.Println(err)
			continue
		}

		for _, existingIndex := range results {
			if existingIndex["name"] == "search" {
				continue insertingIndex
			}
		}
		fmt.Println("Inserting index search for collection " + v)

		res, err := indexes.CreateOne(ctx, model)
		fmt.Println(res, err)

	}
}

func (ms *CatalogMongoService) GetProvider(ctx context.Context, providerType outypes.ProviderType, ProviderId string) (ProviderInternalInfo, error) {
	ctx, span := otel.Tracer("catalog/service").Start(ctx, "GetProvider")
	defer span.End()
	searchCol := ms.Database.Collection(string(providerType))
	var providerFound ProviderInternalInfo
	err := searchCol.FindOne(ctx, bson.D{{Key: "providerId", Value: ProviderId}}).Decode(&providerFound)
	return providerFound, err
}
