package service

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	intypes "gitlab.com/o-cloud/catalog/api/intern"
	outypes "gitlab.com/o-cloud/catalog/api/public"
)

type testSetup struct {
	t      *testing.T
	engine *gin.Engine
	mcs    *MockCatalog
	req    *http.Request
	rr     *httptest.ResponseRecorder
}

func getSetup(t *testing.T, method, url string, body interface{}) testSetup {

	req, err := http.NewRequest(method, url, nil)
	if body != nil {
		byts, _ := json.Marshal(body)
		req, err = http.NewRequest(method, url, bytes.NewBuffer(byts))
	}
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	r := gin.New()
	mcs := &MockCatalog{}
	ch := &CatalogHandler{PublicApi: mcs, InternalApi: mcs}
	ch.SetupPublicRoutes(r.Group("/public"))
	ch.SetupInternalRoutes(r.Group("/intern"))

	//Pré setup de données
	mcs.called = ""
	mcs.ouRetArray = []outypes.ProviderInfo{{Name: "name", Type: "type", ProviderId: "providerId"}}
	mcs.inRetArray = []intypes.ProviderInternalInfo{{Name: "name", Type: "type", ProviderId: "providerId", Visibility: []string{"group"}}}

	return testSetup{
		t:      t,
		engine: r,
		mcs:    mcs,
		req:    req,
		rr:     rr,
	}
}

func TestHandleSearch(t *testing.T) {
	test := getSetup(t, "GET", "/public/typ/search?terms=term", nil)
	test.run()
	test.expectMethodCall(" Search(context.Background,typ,term,)")
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`[{"id":"","providerId":"providerId","providerType":"type","version":"","name":"name","description":"","metadata":null,"endpointLocation":"","workflowStructure":""}]`)
}

func TestHandleGetProviderInfo(t *testing.T) {
	test := getSetup(t, "GET", "/public/typ/provider/provId/info", nil)
	test.run()
	test.expectMethodCall(" GetProviderInfo(context.Background,typ,provId)")
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`{"id":"","providerId":"providerId","providerType":"type","version":"","name":"name","description":"","metadata":null,"endpointLocation":"","workflowStructure":""}`)
}

func TestHandleRegisterDataProvider(t *testing.T) {
	test := getSetup(t, "POST", "/intern/typ/register", intypes.ProviderInternalInfo{Name: "name", Type: "type", ProviderId: "providerId", Visibility: []string{"group"}})
	test.run()
	test.expectMethodCall(" RegisterDataProvider(context.Background,{ providerId [group] type  name  map[]  })")
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`OK`)
}

func TestHandleSearchExistingProvider(t *testing.T) {
	test := getSetup(t, "POST", "/intern/typ/provider/search", intypes.ProviderInternalInfo{Name: "name", Type: "type", ProviderId: "providerId", Visibility: []string{"group"}})
	test.run()
	test.expectMethodCall(" SearchExistingProvider(context.Background,{ providerId [group] type  name  map[]  })")
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`{"id":"","providerId":"providerId","visibility":["group"],"providerType":"type","version":"","name":"name","description":"","metadata":null,"endpointLocation":"","workflowStructure":""}`)
}

func TestHandleRemoveProvider(t *testing.T) {
	test := getSetup(t, "DELETE", "/intern/typ/provider/provId", nil)
	test.run()
	test.expectMethodCall(" RemoveProvider(context.Background, typ, provId)")
	test.expectResponseStatus(http.StatusOK)
}

func TestHandleGetProviderVisibility(t *testing.T) {
	test := getSetup(t, "GET", "/intern/typ/provider/provId/visibility", nil)
	test.run()
	test.expectMethodCall(" GetProviderVisibility(context.Background, typ, provId)")
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`["group"]`)
}

func TestHandleAddProviderVisibility(t *testing.T) {
	test := getSetup(t, "POST", "/intern/typ/provider/provId/visibility", []string{"yeee"})
	test.run()
	test.expectMethodCall(" AddProviderVisibility(context.Background, typ, provId, [yeee])")
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`OK`)
}

func TestHandleRegisterRemoveProviderVisibility(t *testing.T) {
	test := getSetup(t, "DELETE", "/intern/typ/provider/provId/visibility", []string{"yeee"})
	test.run()
	test.expectMethodCall(" RemoveProviderVisibility(context.Background, typ, provId, [yeee])")
	test.expectResponseStatus(http.StatusOK)
	test.expectBodyContent(`OK`)
}

//Helper func
func (ts *testSetup) run() {
	ts.engine.ServeHTTP(ts.rr, ts.req)
}

// Check the response code is what we expect.
func (ts *testSetup) expectResponseStatus(expectedCode int) {
	if ts.rr.Code != expectedCode {
		ts.t.Errorf("handler returned wrong status code: got %v want %v",
			ts.rr.Code, expectedCode)
	}
}

// Check the response body is what we expect.
func (ts *testSetup) expectBodyContent(expectedBody string) {
	if ts.rr.Body.String() != expectedBody {
		ts.t.Errorf("handler returned unexpected body: got %v want %v",
			ts.rr.Body.String(), expectedBody)
	}
}

// Check the response body is what we expect.
func (ts *testSetup) expectMethodCall(expectedMethod string) {
	if ts.mcs.called != expectedMethod {
		ts.t.Errorf("expected method not called: got %v want %v",
			ts.mcs.called, expectedMethod)
	}
}

//Mock calc
type MockCatalog struct {
	called     string
	ouRetArray []outypes.ProviderInfo
	inRetArray []intypes.ProviderInternalInfo
	err        error
}

func (mcs *MockCatalog) Search(ctx context.Context, providerType outypes.ProviderType, searchTerms, originPeer string) ([]outypes.ProviderInfo, error) {
	mcs.called = fmt.Sprintf("%s Search(%s,%s,%s,%s)", mcs.called, ctx, providerType, searchTerms, originPeer)
	return mcs.ouRetArray, mcs.err
}

func (mcs *MockCatalog) GetProviderInfo(ctx context.Context, providerType outypes.ProviderType, providerId string) (outypes.ProviderInfo, error) {
	mcs.called += fmt.Sprintf(" GetProviderInfo(%s,%s,%s)", ctx, providerType, providerId)
	return mcs.ouRetArray[0], mcs.err
}

func (mcs *MockCatalog) RegisterDataProvider(ctx context.Context, provider intypes.ProviderInternalInfo) error {
	mcs.called = mcs.called + fmt.Sprintf(" RegisterDataProvider(%s,%s)", ctx, provider)
	return mcs.err
}

func (mcs *MockCatalog) RemoveProvider(ctx context.Context, providerType outypes.ProviderType, providerId string) error {
	mcs.called = mcs.called + fmt.Sprintf(" RemoveProvider(%s, %s, %s)", ctx, providerType, providerId)
	return mcs.err
}

func (mcs *MockCatalog) SearchExistingProvider(ctx context.Context, provider intypes.ProviderInternalInfo) (intypes.ProviderInternalInfo, error) {
	mcs.called = mcs.called + fmt.Sprintf(" SearchExistingProvider(%s,%s)", ctx, provider)
	return mcs.inRetArray[0], mcs.err
}
func (mcs *MockCatalog) GetProviderVisibility(ctx context.Context, providerType outypes.ProviderType, providerId string) ([]string, error) {
	mcs.called = mcs.called + fmt.Sprintf(" GetProviderVisibility(%s, %s, %s)", ctx, providerType, providerId)
	return mcs.inRetArray[0].Visibility, mcs.err
}

func (mcs *MockCatalog) AddProviderVisibility(ctx context.Context, providerType outypes.ProviderType, providerId string, visibility []string) error {
	mcs.called = mcs.called + fmt.Sprintf(" AddProviderVisibility(%s, %s, %s, %s)", ctx, providerType, providerId, visibility)
	return mcs.err
}

func (mcs *MockCatalog) RemoveProviderVisibility(ctx context.Context, providerType outypes.ProviderType, providerId string, visibility []string) error {
	mcs.called = mcs.called + fmt.Sprintf(" RemoveProviderVisibility(%s, %s, %s, %s)", ctx, providerType, providerId, visibility)
	return mcs.err
}
