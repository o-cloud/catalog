package service

import (
	"context"
	"fmt"
	"testing"

	intypes "gitlab.com/o-cloud/catalog/api/intern"
	outypes "gitlab.com/o-cloud/catalog/api/public"
)

type testPublicSetup struct {
	t       *testing.T
	mm      *MockMongo
	service *CatalogService
}

func GetSetup(t *testing.T) testPublicSetup {

	mock := &MockMongo{}
	return testPublicSetup{t: t, mm: mock, service: &CatalogService{mock}}

}

func TestInterface(t *testing.T) {
	setup := GetSetup(t)
	setup.service.Search(context.TODO(), "typ", "sentinel", "")
	setup.expectMethodCall(" Search(typ,sentinel)")
}

//Testing helpers

// Check the response body is what we expect.
func (ts *testPublicSetup) expectMethodCall(expectedMethod string) {
	if ts.mm.called != expectedMethod {
		ts.t.Errorf("expected method not called: got %v want %v",
			ts.mm.called, expectedMethod)
	}
}

//mock

type MockMongo struct {
	called       string
	providerInfo ProviderInternalInfo
	err          error
}

func (mm *MockMongo) Search(ctx context.Context, providerType outypes.ProviderType, searchTerms, originPeerName string) []outypes.ProviderInfo {
	mm.called = fmt.Sprintf("%s Search(%s,%s)", mm.called, providerType, searchTerms)
	return []outypes.ProviderInfo{*mm.providerInfo.ToPublicApi()}
}
func (mm *MockMongo) GetProvider(ctx context.Context, providerType outypes.ProviderType, ProviderId string) (ProviderInternalInfo, error) {
	return mm.providerInfo, mm.err
}
func (mm *MockMongo) SearchExistingProvider(ctx context.Context, provider intypes.ProviderInternalInfo) (intypes.ProviderInternalInfo, error) {
	return *mm.providerInfo.ToInternalApi(), mm.err
}
func (mm *MockMongo) SaveDataProvider(ctx context.Context, providerType outypes.ProviderType, provider ProviderInternalInfo) error {
	return mm.err
}
func (mm *MockMongo) RemoveProvider(ctx context.Context, providerType outypes.ProviderType, providerId string) error {
	return mm.err
}
func (mm *MockMongo) UpdateVisibility(ctx context.Context, providerType outypes.ProviderType, providerId string, visibility []string) error {
	return mm.err
}
