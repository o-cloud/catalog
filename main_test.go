package main

import (
	"encoding/json"
	"fmt"
	"testing"

	"github.com/gin-gonic/gin"
	"gitlab.com/o-cloud/catalog/service"
)

func TestRoutesSetup(t *testing.T) {
	r := gin.Default()
	catalogHandler := service.CatalogHandler{}
	catalogHandler.SetupPublicRoutes(r.Group("/catalog/public"))
	catalogHandler.SetupInternalRoutes(r.Group("/catalog/internal"))

	routesToMatch := []string{
		"/catalog/public/:type/search",
		"/catalog/public/:type/provider/:providerId/info",
		"/catalog/internal/:providerType/register",
		"/catalog/internal/:providerType/provider/search",
		"/catalog/internal/:providerType/provider/:providerId/visibility"}
	matchedRoute := 0
	for _, v := range r.Routes() {
		for _, match := range routesToMatch {
			if v.Path == match {
				matchedRoute++
			}
		}
	}
	if matchedRoute != 7 {
		t.Error("Error during route matching ", matchedRoute, len(routesToMatch))
	}
}

func PrettyPrint(v interface{}) (err error) {
	b, err := json.MarshalIndent(v, "", "  ")
	if err == nil {
		fmt.Println(string(b))
	}
	return
}
